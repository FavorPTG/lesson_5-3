package com.example.lesson_23;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity implements Runnable {

    private TextView textView;
    private TextView metaData;
    private SeekBar seekBar;
    private FloatingActionButton btnPlay;
    private FloatingActionButton btnFlashForward;
    private FloatingActionButton btnFlashBack;
    private FloatingActionButton btnLoop;
    private FloatingActionButton btnNextTrack;
    private FloatingActionButton btnPreviousTrack;
    private MediaPlayer mp;
    private boolean wasPlaying = false;
    private String metaDataString = "";
    private boolean isLooping = false;

    private int currentTrack = 0;
    private String[] tracks = {"Gotika_-_Gotika_1_(iPleer.com).mp3", "GTA IV - The Theme From Grand Theft Auto IV(Музыка из гта 4).mp3", "GTA_San_Andreas_-_Theme_song_62938231.mp3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        textView = (TextView) findViewById(R.id.textView);
        metaData = (TextView) findViewById(R.id.trackName);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        btnPlay = (FloatingActionButton) findViewById(R.id.playButton);
        btnFlashForward = (FloatingActionButton) findViewById(R.id.flashForwardButton);
        btnFlashBack = (FloatingActionButton) findViewById(R.id.flashBackButton);
        btnLoop = (FloatingActionButton) findViewById(R.id.loopButton);
        btnPreviousTrack = (FloatingActionButton) findViewById(R.id.previousTrackButton);
        btnNextTrack = (FloatingActionButton) findViewById(R.id.nextTrackButton);

        btnPlay.setOnClickListener(buttonsListener);
        btnFlashBack.setOnClickListener(buttonsListener);
        btnFlashForward.setOnClickListener(buttonsListener);
        btnNextTrack.setOnClickListener(buttonsListener);
        btnPreviousTrack.setOnClickListener(buttonsListener);
        btnLoop.setOnClickListener(buttonsListener);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int timeTrack = (int) Math.ceil(progress / 1000f);

                // (timeTrack > 60 ? (timeTrack-(60*(timeTrack/60))) : timeTrack)
                textView.setText((int)(timeTrack / 60) + ":" + (timeTrack > 60 ? (timeTrack-(60*(timeTrack/60))) : timeTrack));


                double precentTrack = progress / (double) seekBar.getMax();
                //textView.setX(seekBar.getX() + Math.round(seekBar.getWidth()*precentTrack*0.92));

                if (progress > 0 && mp != null && !mp.isPlaying()) { // если mediaPlayer не пустой и mediaPlayer не воспроизводится
                    cleanResMedia(); // остановка и очиска MediaPlayer
                    // назначение кнопке картинки play
                    btnPlay.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_play));
                    MainActivity.this.seekBar.setProgress(0); // установление seekBar значения 0
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(mp != null && mp.isPlaying()){
                    mp.seekTo(seekBar.getProgress());
                }
            }
        });
    }

//    View.OnClickListener clickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            playMusic();
//        }
//    };

    View.OnClickListener buttonsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                switch(v.getId()){
                    case R.id.playButton:
                        playMusic();
                        break;
                    case R.id.flashBackButton:
                        mp.seekTo(mp.getCurrentPosition()-5000);
                        seekBar.setProgress(mp.getCurrentPosition());
                        break;
                    case R.id.flashForwardButton:
                        mp.seekTo(mp.getCurrentPosition()+5000);
                        seekBar.setProgress(mp.getCurrentPosition());
                        break;
                    case R.id.previousTrackButton:
                        if(wasPlaying)
                            wasPlaying = false;

                        if(mp != null && mp.isPlaying())
                            cleanResMedia();

                        seekBar.setProgress(0);
                        currentTrack = (currentTrack-1 < 0 ? tracks.length-1 : currentTrack-1);
                        playMusic();

                        break;
                    case R.id.nextTrackButton:
                        if(wasPlaying)
                            wasPlaying = false;

                        if(mp != null && mp.isPlaying())
                            cleanResMedia();

                        seekBar.setProgress(0);
                        currentTrack = (currentTrack+1 >= tracks.length ? 0 : currentTrack+1);
                        playMusic();

                        break;
                    case R.id.loopButton:
                        if(mp != null)
                            if(isLooping){
                                mp.setLooping(false);
                                isLooping = false;
                            } else{
                                mp.setLooping(true);
                                isLooping = true;
                            }
                        break;
                }
            } catch (Exception | Error e){
                Toast.makeText(MainActivity.this, "Ничего не воспроизводится", Toast.LENGTH_LONG).show();
            }
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        cleanResMedia();
    }

    private void cleanResMedia(){
        mp.stop();
        mp.release();  // Очистка медиа-плеера
        mp = null;
    }

    private void playMusic(){
        try{
            if(mp != null && mp.isPlaying()){
                cleanResMedia();
                //seekBar.setProgress(0);
                wasPlaying = true;
                btnPlay.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_play));  // Установка иконки воспроизведения для кнопки
            }
            if(!wasPlaying) {
                if (mp == null) {
                    mp = new MediaPlayer();
                }
                btnPlay.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, android.R.drawable.ic_media_pause));  // Установка иконки паузы для кнопки

                AssetFileDescriptor descriptor = getAssets().openFd(tracks[currentTrack]);  // Получение файла и информации о нём
                mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());  // Передаём файл в медиа-плеер
                MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();
                metadataRetriever.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());  // Запись метаданных из файла (дескриптор, начало файла, конец файла)
                metaDataString = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                metaDataString += "\n" + metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                metadataRetriever.release();
                descriptor.close();

                metaData.setText(metaDataString);  // Вывод данных на экран

                mp.prepare();  // Подгатовка плеера
                mp.setLooping(isLooping);  // Отключение повторения

                seekBar.setMax(mp.getDuration());  // Установка максимального значения для бара
                mp.seekTo(seekBar.getProgress());

                new Thread(this).start();  // Старт нового потока

                mp.start();
            }
            if(wasPlaying){
                wasPlaying = false;
            }
        } catch(IOException e){
            Toast.makeText(this, "Файл не найден...", Toast.LENGTH_SHORT).show();
        } catch (Error e){
            Toast.makeText(this, "Произошла ошибка...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void run() {
        int currentPosition = mp.getCurrentPosition();  // Получение текущего положения в треке
        int total = mp.getDuration();  // Получение всей длины трека

        while(mp != null && mp.isPlaying() && currentPosition < total){
            try {
                Thread.sleep(100);
                currentPosition = mp.getCurrentPosition();
            } catch (InterruptedException e) {
                Toast.makeText(this, "Error...", Toast.LENGTH_SHORT).show();
                return;
            } catch (Exception e){
                return;
            }
            seekBar.setProgress(currentPosition);
        }
    }
}